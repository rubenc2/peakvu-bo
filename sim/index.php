<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
	<link rel="canonical" href="bochecker/simulator.php">
	<meta name="robots" content="index, follow">
    <link rel="shortcut icon" type="image/png" href="favicon.png">
    
	<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?6631">
	<link rel="stylesheet" type="text/css" href="style.css?1596">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <script>
          $(document).ready(function() {
            setInterval(function() {
              $("#bloc-15").load(window.location.href + " #bloc-15");
	    }, 10000);
          });
	</script>

    <title>Simulator</title>


    
<!-- Analytics -->
 
<!-- Analytics END -->
    
</head>
<body>

<!-- Preloader -->
<div id="page-loading-blocs-notifaction" class="page-preloader"></div>
<!-- Preloader END -->


<!-- Main container -->
<div class="page-container">
    
<!-- bloc-0 -->
<div class="bloc l-bloc" id="bloc-0">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col">
				<nav class="navbar navbar-light row navbar-expand-md flex-column" role="navigation">
					<a class="navbar-brand mx-auto" href="index.php"><picture><source type="image/webp" srcset="img/favicon.webp"><img src="img/favicon.png" alt="logo" width="250" height="101"></picture></a>
					<button id="nav-toggle" type="button" class="ui-navbar-toggler navbar-toggler border-0 p-0" data-toggle="collapse" data-target=".navbar-34817" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"><svg height="32" viewBox="0 0 32 32" width="32"><path class="svg-menu-icon " d="m2 9h28m-28 7h28m-28 7h28"></path></svg></span>
					</button>
					<div class="collapse navbar-collapse navbar-34817">
							<ul class="site-navigation nav navbar-nav mx-auto justify-content-center">
                                                        <li class="nav-item">
                                                                <a href="../setup.php" class="nav-link a-btn ltc-4866">Setup</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a href="../services.php" class="nav-link a-btn ltc-4866">Services</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a href="../calendar.php" class="nav-link a-btn ltc-4866">Schedule</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a href="../bo/index.php" class="nav-link a-btn ltc-4866">QXC</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a href="../logs.php" class="nav-link a-btn ltc-4866">Logs</a>
							</li>
                                                        <li class="nav-item">
                                                                <a href="index.php" class="nav-link a-btn ltc-4866">Simulator</a>
                                                        </li>
							</ul>
						</div>
				</nav>
			</div>
		</div>
	</div>
</div>
<!-- bloc-0 END -->

<!-- bloc-1 -->
<div class="bloc l-bloc" id="bloc-1">
	<div class="container bloc-lg bloc-sm-lg">
		<div class="row">
			<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-left">
				<h1 class="mb-4 text-lg-center tc-4866">
					QXC
				</h1>
			</div>
		</div>
	</div>
</div>
<!-- bloc-1 END -->

<?php
$px=trim(shell_exec('cat px'));
?>

<?php if ($px == '0'): ?>
<!-- bloc-12 -->
<div class="bloc l-bloc" id="bloc-12">
	<div class="container bloc-lg bloc-lg-lg">
		<div class="row">
			<div class="col-md-4 text-left text-lg-center">
				<h3 class="tc-5999">
					DMA
				</h3>
			</div>
			<div class="col-md-4 text-left text-lg-center">
				<h3 class="tc-5999">
					Service
				</h3>
			</div>
			<div class="col-md-4 text-left text-lg-center">
				<h3 class="tc-5999">
					Replacement
				</h3>
			</div>
		</div>
	</div>
</div>
<!-- bloc-12 END -->

<!-- bloc-13 -->
<form action="startSim.php" method="get">
<div class="bloc l-bloc" id="bloc-13">
    <div class="container bloc-lg bloc-no-padding-lg">
        <div class="row">
                <div class="col-md-4 text-left">
                    <div class="form-group">
                        <select class="form-control" name="dma">
                            <option value="dma_1">
                                Fort Myers
                            </option>
                            <option value="dma_3">
                                Orlando
                            </option>
                            <option value="dma_2">
                                Miami
                            </option>
                            <option value="dma_1">
                                Tampa
                            </option>
                            <option value="dma_4">
                                West PB
                            </option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 text-left">
                    <div class="form-group">
                        <select class="form-control" name="service">
                            <?php
                            $services_file = file("../tmp/services");
                            $services_added = [];
                            foreach ($services_file as $service_line) {
                                $service_line = trim($service_line);
                                list($service_option, $service_value) = explode(",", $service_line);
                                $base_service_name = preg_replace('/(_DASH|_HLS)$/', '', $service_option);
                                if (!in_array($base_service_name, $services_added)) {
                                    echo "<option value='{$base_service_name}'>{$base_service_name}</option>";
                                    $services_added[] = $base_service_name;
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 text-left">
                    <div class="form-group">
                        <select class="form-control" name="source">
                            <?php
                            $sources_file = file("../tmp/sources");
                            $sources_added = [];
                            foreach ($sources_file as $source_line) {
                                $source_line = trim($source_line);
                                list($source_option, $source_value) = explode(",", $source_line);
                                $base_source_name = preg_replace('/(_DASH|_HLS|_Alt_\d+)$/', '', $source_option);
                                if (!in_array($base_source_name, $sources_added)) {
                                    echo "<option value='{$base_source_name}'>{$base_source_name}</option>";
                                    $sources_added[] = $base_source_name;
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
        </div>
    </div>
</div>
<!-- bloc-13 END -->


<!-- bloc-14 -->
<div class="bloc l-bloc" id="bloc-14">
	<div class="container bloc-lg bloc-sm-lg">
		<div class="row">
			<div class="col text-left">
				<div class="divider-h">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-14 END -->

<!-- bloc-15 -->
<div class="bloc l-bloc" id="bloc-15">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-left">
				<button class="btn btn-lg btn-c-5384 btn-block bloc-button" type="submit">
					Start Simulation
				</button>
			</div>
		</div>
	</div>
</div>
<!-- bloc-15 END -->

<!-- bloc-16 -->
<div class="bloc l-bloc" id="bloc-16">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col text-left">
				<div class="divider-h">
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<!-- bloc-16 END -->

<?php else: ?>

<!-- bloc-15 -->
<!--
<div class="bloc l-bloc " id="bloc-15">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-left">
				<div class="card" style="width: 140%">
					<div class="card-body">

                                        <pre text-align: left>
                                         <code class="language-xml"><?php  $body = shell_exec('cat body'); echo htmlspecialchars($body); ?></code>
			                </pre>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-15 END -->

<div class="bloc l-bloc" id="bloc-15" name="bloc-15">
    <div class="container bloc-lg">
        <div class="row">
            <div class="col text-left" style="display: flex; justify-content: center;">
                <div class="card" style="width: 70%;">
                    <div class="card-body">
                        <pre text-align: left>
                            <code class="language-xml"><?php $body = shell_exec('cat body'); echo htmlspecialchars($body); ?></code>
                        </pre>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- bloc-16 -->
<div class="bloc l-bloc" id="bloc-16">
	<div class="container bloc-lg bloc-sm-lg">
		<div class="row">
			<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-left">
                        <?php
                         $bo = trim(shell_exec('cat bo'));
                         if($bo == 0){
				echo '<a href="blackout.php" class="btn btn-lg btn-c-902 btn-block">Blackout</a>';
                         }else{
				 echo '<a href="normalize.php" class="btn btn-lg btn-c-3482 btn-block">Normalize</a>';
		         }
                         ?>
			</div>
		</div>
	</div>
</div>
<!-- bloc-16 END -->

<!-- bloc-17 -->
<div class="bloc l-bloc" id="bloc-17">
	<div class="container bloc-lg bloc-sm-lg">
		<div class="row">
			<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-left">
<?php $bo = trim(shell_exec('cat bo')); if($bo == 0){echo'	<a href="stop.php" class="btn btn-lg btn-block btn-c-5384">Stop Simulation</a>';} ?>
			</div>
		</div>
	</div>
</div>
<!-- bloc-17 END -->

<!-- ScrollToTop Button -->
<button aria-label="Scroll to top button" class="bloc-button btn btn-d scrollToTop" onclick="scrollToTarget('1',this)"><svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 32 32"><path class="scroll-to-top-btn-icon" d="M30,22.656l-14-13-14,13"/></svg></button>
<!-- ScrollToTop Button END-->


</div>
<!-- Main container END -->
    
<?php endif; ?>

<!-- Additional JS -->
<script src="./js/jquery.min.js?5652"></script>
<script src="./js/bootstrap.bundle.min.js?6814"></script>
<script src="./js/blocs.min.js?8786"></script>
<script src="./js/lazysizes.min.js" defer></script><!-- Additional JS END -->


</body>
</html>
