#!/bin/bash
#blackout similator
px=`cat /var/www/html/sim/px | awk '{print $1*1}'`

service=`cat /var/www/html/sim/service | tr -d "'"`
source=`cat /var/www/html/sim/source | tr -d "'"`
apiKey=`cat /var/www/html/keyfile | tr -d "'"`

sid=$(printf "%03d" $((RANDOM % 1000)))
time=$(date -u +"%Y-%m-%dT%H:%M:%SZ" --date "+10 seconds")

dma_content=$(cat /var/www/html/sim/$(cat /var/www/html/sim/dma | tr -d "'"))


while [[ px -eq 1 ]]; do

	active=`cat /var/www/html/sim/bo | awk '{print $1*1}'`
	if [[ $active -eq 0 ]]; then
		sourcebo=$(echo $service)
	else
		sourcebo=$(echo $source)
	fi
        #sid="139"
        time=$(date -u +"%Y-%m-%dT%H:%M:%SZ" --date "+10 seconds")
        body="<Media href=\"${service}\">
  <MediaPoint id=\"sid_${sid}\" matchTime=\"${time}\" expectedDuration=\"PT3H\">
    <Apply>
      <Policy id=\"1\">
        <ViewingPolicy id=\"1\">
          ${dma_content}
          <action:Content>${sourcebo}</action:Content>
        </ViewingPolicy>
      </Policy>
    </Apply>
  </MediaPoint>
</Media>"

	curl -s --request PUT --url "https://api.broadpeak.io/v1/esni/media/sid_${sid}" --header "authorization: Bearer ${apiKey}" --header "content-type: application/xml" --data "${body}" | jq '.' > /tmp/ensi.json

	error=`cat /tmp/ensi.json | jq -r .error`
	message=`cat /tmp/ensi.json | jq -r .message`
	ltime=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
	if [[ ${error} == "null" ]] || [[ -z ${error} ]]; then
		echo "INFO: ${ltime},sid_${sid},OK" >> ../main.log
		echo "${body}" >> ../main.log
	else
	    echo "ERROR: ${ltime},sid_${sid},${error},${message}</span>" >> ../main.log
	fi
	echo "----------<>----------" >> ../main.log

        echo -e "\n${body}" > /var/www/html/sim/body

	px=`cat /var/www/html/sim/px | awk '{print $1*1}'`

	sleep 50

done
