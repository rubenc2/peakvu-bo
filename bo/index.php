<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
    <link rel="canonical" href="bochecker/">
    <meta name="robots" content="index, follow">
    <link rel="shortcut icon" type="image/png" href="favicon.png">
    
    <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?6948">
    <link rel="stylesheet" type="text/css" href="style.css?8873">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<!--
        <script>
          $(document).ready(function() {
            setInterval(function() {
              $("#bloc-4").load(window.location.href + " #bloc-4");
	    }, 30000);
          });
        </script>
-->
<script>
  $(document).ready(function() {
    // Initialize tooltips
    $('[data-toggle="tooltip"]').tooltip();

    setInterval(function() {
      $("#bloc-4").load(window.location.href + " #bloc-4", function() {
        // Re-initialize tooltips after content update
        $('[data-toggle="tooltip"]').tooltip();
      });
    }, 30000);
  });
</script>

     <title>PeakVu-BO</title>

    
<!-- Analytics -->
 
<!-- Analytics END -->
    
</head>
<body>

<!-- Preloader -->
<div id="page-loading-blocs-notifaction" class="page-preloader"></div>
<!-- Preloader END -->


<!-- Main container -->
<div class="page-container">
    
<!-- bloc-0 -->
<div class="bloc l-bloc" id="bloc-0">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col">
				<nav class="navbar navbar-light row navbar-expand-md flex-column" role="navigation">
					<a class="navbar-brand mx-auto" href="index.php"><picture><source type="image/webp" srcset="img/favicon.webp"><img src="img/favicon.png" alt="logo" width="250" height="101"></picture></a>
					<button id="nav-toggle" type="button" class="ui-navbar-toggler navbar-toggler border-0 p-0" data-toggle="collapse" data-target=".navbar-34817" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"><svg height="32" viewBox="0 0 32 32" width="32"><path class="svg-menu-icon " d="m2 9h28m-28 7h28m-28 7h28"></path></svg></span>
					</button>
					<div class="collapse navbar-collapse navbar-34817">
							<ul class="site-navigation nav navbar-nav mx-auto justify-content-center">
                                                        <li class="nav-item">
                                                                <a href="../setup.php" class="nav-link a-btn ltc-4866">Setup</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a href="../services.php" class="nav-link a-btn ltc-4866">Services</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a href="../calendar.php" class="nav-link a-btn ltc-4866">Schedule</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a href="index.php" class="nav-link a-btn ltc-4866">QXC</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a href="../logs.php" class="nav-link a-btn ltc-4866">Logs</a>
							</li>
                                                        <li class="nav-item">
                                                                <a href="../sim/index.php" class="nav-link a-btn ltc-4866">Simulator</a>
                                                        </li>
							</ul>
						</div>
				</nav>
			</div>
		</div>
	</div>
</div>
<!-- bloc-0 END -->

<!-- bloc-1 -->
<div class="bloc l-bloc" id="bloc-1">
	<div class="container bloc-lg bloc-sm-lg">
		<div class="row">
			<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-left">
				<h1 class="mb-4 text-lg-center tc-4866">
					QxC
				</h1>
			</div>
		</div>
	</div>
</div>
<!-- bloc-1 END -->

<?php
$ch = shell_exec('cat ch');
      if($ch == 0){
              echo '
<!-- bloc-1 -->
<div class="bloc tc-4866 l-bloc" id="bloc-1">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-md-left text-center">
				<form id="form_1" data-form-type="blocs-form" action="get_url.php" method="GET">
					<div class="form-group">
						<label class="label-hls-url-style">
							Channel URL (M3U8) - broadpeak.io HLS Service URL
						</label>
						<input id="url" name="url" class="form-control" required data-placement="right" data-toggle="tooltip" title="This is the broadpeak.io service URL of the Channel that needs to be monitored for blackout" data-validation-required-message="The HLS URL is required">
					</div>
					<div class="divider-h">
					</div> 
					<button class="bloc-button btn btn-lg btn-block btn-c-5384 btn-rd" type="submit">
						Submit
					</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- bloc-1 END -->';
      }
?>


<?php
$ch = shell_exec('cat ch');
if($ch != 1){
	exit();
}
?>

<div>
        <div>
                <div>
                        <div>
                                <h2 class="mb-4 text-lg-center tc-4866">
<?php $sname=shell_exec('cat tmp/sname'); echo "Service: $sname"; ?>
                                </h2>
                        </div>
                </div>
        </div>
</div>

<!-- bloc-4 -->
<div class="bloc l-bloc " id="bloc-4" name="bloc-4">
	<div class="container bloc-lg bloc-lg-lg">
		<div class="row">
			<div class="col-md-2 text-left">
				<label>
					Label
				</label>
				<h4 class="mb-4 text-lg-center tc-4866">
					Fort Myers:
				</h4><?php $cl=shell_exec('cat tmp/34223 | cut -d, -f1'); $se=shell_exec('cat tmp/34223 | cut -d, -f2'); echo "<a href='index.php' class='btn btn-lg btn-block ${cl}' data-placement='right' data-toggle='tooltip' title='${se}'>34223</a>";?>
			</div>
			<div class="col-md-2 text-left">
				<label>
					Label
				</label>
				<h4 class="mb-4 text-lg-center tc-4866">
					Orlando:
				</h4><?php $cl=shell_exec('cat tmp/32169 | cut -d, -f1'); $se=shell_exec('cat tmp/32169 | cut -d, -f2'); echo "<a href='index.php' class='btn btn-lg btn-block ${cl}' data-placement='right' data-toggle='tooltip' title='${se}'>32169</a>";?>
				<?php $cl=shell_exec('cat tmp/32118 | cut -d, -f1'); $se=shell_exec('cat tmp/32118 | cut -d, -f2'); echo "<a href='index.php' class='btn btn-lg btn-block ${cl}' data-placement='right' data-toggle='tooltip' title='${se}'>32118</a>";?>
			</div>
			<div class="col-md-2 text-left">
				<label>
					Label
				</label>
				<h4 class="mb-4 text-lg-center tc-4866">
					Miami:
				</h4><?php $cl=shell_exec('cat tmp/33018 | cut -d, -f1'); $se=shell_exec('cat tmp/33018 | cut -d, -f2'); echo "<a href='index.php' class='btn btn-lg btn-block ${cl}' data-placement='right' data-toggle='tooltip' title='${se}'>33018</a>";?>
				<?php $cl=shell_exec('cat tmp/33062 | cut -d, -f1'); $se=shell_exec('cat tmp/33062 | cut -d, -f2'); echo "<a href='index.php' class='btn btn-lg btn-block ${cl}' data-placement='right' data-toggle='tooltip' title='${se}'>33062</a>";?>
			</div>
			<div class="col-md-2 text-left">
				<label>
					Label
				</label>
				<h4 class="mb-4 text-lg-center tc-4866">
					Tampa:
				</h4><?php $cl=shell_exec('cat tmp/34223 | cut -d, -f1'); $se=shell_exec('cat tmp/34223 | cut -d, -f2'); echo "<a href='index.php' class='btn btn-lg btn-block ${cl}' data-placement='right' data-toggle='tooltip' title='${se}'>34223</a>";?>
			</div>
			<div class="col-md-2 text-left">
				<label>
					Label
				</label>
				<h4 class="mb-4 text-lg-center tc-4866">
					West PB:
				</h4><?php $cl=shell_exec('cat tmp/33483 | cut -d, -f1'); $se=shell_exec('cat tmp/33483 | cut -d, -f2'); echo "<a href='index.php' class='btn btn-lg btn-block ${cl}' data-placement='right' data-toggle='tooltip' title='${se}'>33483</a>";?>
				<?php $cl=shell_exec('cat tmp/33477 | cut -d, -f1'); $se=shell_exec('cat tmp/33477 | cut -d, -f2'); echo "<a href='index.php' class='btn btn-lg btn-block ${cl}' data-placement='right' data-toggle='tooltip' title='${se}'>33477</a>";?>
				<?php $cl=shell_exec('cat tmp/33404 | cut -d, -f1'); $se=shell_exec('cat tmp/33404 | cut -d, -f2'); echo "<a href='index.php' class='btn btn-lg btn-block ${cl}' data-placement='right' data-toggle='tooltip' title='${se}'>33404</a>";?>
				<?php $cl=shell_exec('cat tmp/33410 | cut -d, -f1'); $se=shell_exec('cat tmp/33410 | cut -d, -f2'); echo "<a href='index.php' class='btn btn-lg btn-block ${cl}' data-placement='right' data-toggle='tooltip' title='${se}'>33410</a>";?>
				<?php $cl=shell_exec('cat tmp/33408 | cut -d, -f1'); $se=shell_exec('cat tmp/33408 | cut -d, -f2'); echo "<a href='index.php' class='btn btn-lg btn-block ${cl}' data-placement='right' data-toggle='tooltip' title='${se}'>33408</a>";?>
				<?php $cl=shell_exec('cat tmp/34957 | cut -d, -f1'); $se=shell_exec('cat tmp/34957 | cut -d, -f2'); echo "<a href='index.php' class='btn btn-lg btn-block ${cl}' data-placement='right' data-toggle='tooltip' title='${se}'>34957</a>";?>
				<?php $cl=shell_exec('cat tmp/33487 | cut -d, -f1'); $se=shell_exec('cat tmp/33487 | cut -d, -f2'); echo "<a href='index.php' class='btn btn-lg btn-block ${cl}' data-placement='right' data-toggle='tooltip' title='${se}'>33487</a>";?>
				<?php $cl=shell_exec('cat tmp/33426 | cut -d, -f1'); $se=shell_exec('cat tmp/33426 | cut -d, -f2'); echo "<a href='index.php' class='btn btn-lg btn-block ${cl}' data-placement='right' data-toggle='tooltip' title='${se}'>33426</a>";?>
				<?php $cl=shell_exec('cat tmp/33407 | cut -d, -f1'); $se=shell_exec('cat tmp/33407 | cut -d, -f2'); echo "<a href='index.php' class='btn btn-lg btn-block ${cl}' data-placement='right' data-toggle='tooltip' title='${se}'>33407</a>";?>	
			</div>
		</div>
	</div>
</div>
<!-- bloc-4 END -->

<!-- bloc-5 -->
<!--
<div class="bloc l-bloc" id="bloc-5">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-left">
				<div class="divider-h">
				</div>
			</div>
		</div>
	</div>
</div>
-->
<!-- bloc-5 END -->

<!-- bloc-5 -->
<div class="bloc l-bloc" id="bloc-5">
	<div class="container bloc-lg bloc-sm-lg">
		<div class="row">
			<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-md-left text-center">
				<a href="clear.php" class="btn btn-lg btn-block btn-rd btn-c-5384">Change Channel</a>
			</div>
		</div>
	</div>
</div>
<!-- bloc-5 END -->

<!-- bloc-7 -->
<!--
<div class="bloc l-bloc" id="bloc-7">
	<div class="container bloc-lg bloc-no-padding-lg">
		<div class="row">
			<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 offset-lg-3 text-left col-lg-6">
				<div class="divider-h">
				</div>
			</div>
		</div>
	</div>
</div>
-->
<!-- bloc-7 END -->
<!-- bloc-8 -->
<div class="bloc l-bloc" id="bloc-8">
	<div class="container bloc-lg bloc-no-padding-lg">
		<div class="row">
			<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-left">
				<?php
				$px = shell_exec('cat px');
				if($px == "0") {
					echo '<a href="start.php" class="btn btn-lg btn-block btn-rd btn-c-5999">Start Monitoring</a>';
				}else{
					echo '<a href="stop.php" class="btn btn-lg btn-block btn-rd btn-c-5384">Stop Monitoring</a>';
				}
?>
			</div>
		</div>
	</div>
</div>
<!-- bloc-8 END -->

<!-- bloc-6 -->
<div class="bloc l-bloc" id="bloc-6">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col text-md-left text-center">
				<div class="divider-h">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-6 END -->

<!-- ScrollToTop Button -->
<button aria-label="Scroll to top button" class="bloc-button btn btn-d scrollToTop" onclick="scrollToTarget('1',this)"><svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 32 32"><path class="scroll-to-top-btn-icon" d="M30,22.656l-14-13-14,13"/></svg></button>
<!-- ScrollToTop Button END-->


</div>
<!-- Main container END -->
    


<!-- Additional JS -->
<script src="./js/jquery.min.js?3797"></script>
<script src="./js/bootstrap.bundle.min.js?7008"></script>
<script src="./js/blocs.min.js?8158"></script>
<script src="./js/lazysizes.min.js" defer></script>
<!-- Additional JS END -->


</body>
</html>
