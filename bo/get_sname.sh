#!/bin/bash
#input the link when the script is run
link=$1

path="/var/www/html/bo"

#Download the main manifest
curl --compressed -s -L "${link}" > ${path}/tmp/index.0  2>&1

#Grab the first rendition or bitrate manifest
next=`cat ${path}/tmp/index.0 | grep -v "#" | grep video | grep m3u8 | head -1`
#parse the name of the service
service=$(echo $next | cut -d\- -f1)
echo ${service} > ${path}/tmp/sname
exit 0
