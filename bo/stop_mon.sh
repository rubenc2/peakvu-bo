#!/bin/bash
path="/var/www/html/bo"
sname=`cat ${path}/tmp/sname`
while read code
do
    echo "btn-c-5999,${sname}" > ${path}/tmp/${code}
done < ${path}/qxc_codes
chown www-data:www-data ${path}/tmp/*
chmod 777 ${path}/tmp/*
