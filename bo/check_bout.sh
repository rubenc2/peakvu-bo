#!/bin/bash
#input the link when the script is run
link=$1

path="/var/www/html/bo"

fid=$(echo $link | rev | cut -d\= -f1 | rev)
#Download the main manifest
curl --compressed -s -L "${link}" > ${path}/tmp/index.${fid}  2>&1

#Grab the first rendition or bitrate manifest
next=`cat ${path}/tmp/index.${fid} | grep -v "#" | grep video | grep m3u8 | head -1`
#parse the name of the service
service1=$(echo $next | cut -d\- -f1)
#parse the broadpeak.io hash and delete it from the main url to create the base url
hash=$(echo $link | cut -d\/ -f4)
base=$(echo $link | sed "s/${hash}\///g" | cut -d\/ -f1-6)

#create the second url for the specific rendition or bitrate
link2="${base}/${next}"
#Download the manifest of the specific rendition or bitrate url
curl --compressed -s -L "${link2}" > ${path}/tmp/index2.${fid}  2>&1

#check the response and parse how the segments are constructed
service_check=`cat ${path}/tmp/index2.${fid} | grep -v "#" | tail -1 | cut -d\- -f1`
service_check=${service_check:0:2}

#based on the segment url construction, parse the service name
if [[ ${service_check} == ".." ]]; then
	service2=`cat ${path}/tmp/index2.${fid} | grep -v "#" | tail -1 | cut -d\/ -f5`
else
	service2=`cat ${path}/tmp/index2.${fid} | grep -v "#" | tail -1 | cut -d\- -f1`
fi

#if the service from the parsed bitrate or rendition manifest is different than the one on the main manifest, then there is a blackout
if [[ ${service1} != ${service2} ]]; then
	echo "btn-c-902,${service2}" > ${path}/tmp/${fid}
	#send API to DD
	exit 5
else
	echo "btn-c-4877,${service2}" > ${path}/tmp/${fid}
	#send API to DD
	exit 0
fi
chown www-data:www-data ${path}/tmp/*
chmod 777 ${path}/tmp/*
