<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
	<link rel="canonical" href="bochecker/setup.php">
	<meta name="robots" content="index, follow">
    <link rel="shortcut icon" type="image/png" href="favicon.png">
    
	<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?6948">
	<link rel="stylesheet" type="text/css" href="style.css?8873">
	
    <title>Setup</title>


    
<!-- Analytics -->
 
<!-- Analytics END -->
    
</head>
<body>

<!-- Preloader -->
<div id="page-loading-blocs-notifaction" class="page-preloader"></div>
<!-- Preloader END -->


<!-- Main container -->
<div class="page-container">
    
<!-- bloc-0 -->
<div class="bloc l-bloc" id="bloc-0">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col">
				<nav class="navbar navbar-light row navbar-expand-md flex-column" role="navigation">
					<a class="navbar-brand mx-auto" href="index.php"><picture><source type="image/webp" srcset="img/favicon.webp"><img src="img/favicon.png" alt="logo" width="250" height="101"></picture></a>
					<button id="nav-toggle" type="button" class="ui-navbar-toggler navbar-toggler border-0 p-0" data-toggle="collapse" data-target=".navbar-34817" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"><svg height="32" viewBox="0 0 32 32" width="32"><path class="svg-menu-icon " d="m2 9h28m-28 7h28m-28 7h28"></path></svg></span>
					</button>
					<div class="collapse navbar-collapse navbar-34817">
							<ul class="site-navigation nav navbar-nav mx-auto justify-content-center">
								<li class="nav-item">
									<a href="index.php" class="nav-link a-btn ltc-4866">bochecker_0.10</a>
								</li>
								<li class="nav-item">
									<a href="setup.php" class="a-btn nav-link ltc-4866">Setup</a>
								</li>
							</ul>
						</div>
				</nav>
			</div>
		</div>
	</div>
</div>
<!-- bloc-0 END -->

<!-- bloc-1 -->
<div class="bloc l-bloc" id="bloc-1">
	<div class="container bloc-lg bloc-sm-lg">
		<div class="row">
			<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-left">
				<h1 class="mb-4 text-lg-center tc-4866">
					QXC
				</h1>
			</div>
		</div>
	</div>
</div>
<!-- bloc-1 END -->

<!-- bloc-7 -->
<div class="bloc l-bloc" id="bloc-7">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col text-left">
				<div class="form-group">
					<label class="tc-5999">
						Frequency
					</label>
					<select class="form-control">
						<option value="0">
							Option 1
						</option>
						<option value="1">
							Option 2
						</option>
					</select>
					<div class="divider-h">
					</div>
					<div class="form-group">
						<label class="tc-5999">
							Duration
						</label>
						<select class="form-control">
							<option value="0">
								Option 1
							</option>
							<option value="1">
								Option 2
							</option>
						</select>
						<div class="divider-h">
						</div>
						<div class="form-group">
							<label class="tc-5999">
								DMA
							</label>
							<select class="form-control">
								<option value="0">
									Option 1
								</option>
								<option value="1">
									Option 2
								</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-7 END -->

<!-- bloc-8 -->
<div class="bloc l-bloc" id="bloc-8">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col text-left">
				<div class="text-center">
					<a href="index.php" class="btn btn-c-3482 btn-lg btn-style">Save</a>
				</div>
				<div class="divider-h">
				</div>
				<div class="text-center">
					<a href="index.php" class="btn btn-lg btn-c-3482 btn-apply-style">Apply</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-8 END -->

<!-- ScrollToTop Button -->
<button aria-label="Scroll to top button" class="bloc-button btn btn-d scrollToTop" onclick="scrollToTarget('1',this)"><svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 32 32"><path class="scroll-to-top-btn-icon" d="M30,22.656l-14-13-14,13"/></svg></button>
<!-- ScrollToTop Button END-->


</div>
<!-- Main container END -->
    


<!-- Additional JS -->
<script src="./js/jquery.min.js?3797"></script>
<script src="./js/bootstrap.bundle.min.js?7008"></script>
<script src="./js/blocs.min.js?8158"></script>
<script src="./js/lazysizes.min.js" defer></script><!-- Additional JS END -->


</body>
</html>
