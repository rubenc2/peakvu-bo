#!/bin/bash
#
path="/var/www/html/bo"
px=`cat ${path}/px`

channel=`cat ${path}/tmp/churl`

while [ ${px} == "1" ]
do
    while read code
    do
        ${path}/check_bout.sh ${channel}?zip=${code}
    done < ${path}/qxc_codes

    sleep 30
    px=`cat ${path}/px`

done
${path}/stop_mon.sh > /dev/null 2>&1
exit 0
