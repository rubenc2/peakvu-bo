<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
	<link rel="canonical" href="simsub/">
	<meta name="robots" content="index, follow">
    <link rel="shortcut icon" type="image/png" href="favicon.png">
    
	<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?3072">
	<link rel="stylesheet" type="text/css" href="style.css?6835">
	<link rel="stylesheet" type="text/css" href="./css/all.min.css">

    <link href="assets/css/html5fileupload.css?v1.0" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <script src="assets/js/html5fileupload.js?v1.2.1"></script>
<!--
	<script>
	  $(document).ready(function() {
	    setInterval(function() {
	      $("#bloc-5").load(window.location.href + " #bloc-5");
	    }, 10000);
	  });
	</script>
-->
    <title>PeakVu-BO</title>


    
<!-- Analytics -->
 
<!-- Analytics END -->
    
</head>
<body>

<!-- Preloader -->
<div id="page-loading-blocs-notifaction" class="page-preloader"></div>
<!-- Preloader END -->


<!-- Main container -->
<div class="page-container">
    
<!-- bloc-0 -->
<div class="bloc l-bloc" id="bloc-0">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col">
				<nav class="navbar navbar-light row navbar-expand-md flex-column" role="navigation">
					<a class="navbar-brand mx-auto" href="setup.php"><img src="img/Logotype%20white%20Broadpeak%20rgb.png" alt="logo" /></a>
					<button id="nav-toggle" type="button" class="ui-navbar-toggler navbar-toggler border-0 p-0" data-toggle="collapse" data-target=".navbar-34817" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse navbar-34817">
						<ul class="site-navigation nav navbar-nav mx-auto justify-content-center">
							<li class="nav-item">
								<a href="setup.php" class="nav-link a-btn ltc-light-gray">Setup</a>
							</li>
							<li class="nav-item">
								<a href="services.php" class="nav-link a-btn ltc-light-gray">Services</a>
							</li>
							<li class="nav-item">
								<a href="calendar.php" class="nav-link a-btn ltc-light-gray">Schedule</a>
							</li>
                                                        <li class="nav-item">
                                                                <a href="/bo/index.php" class="nav-link a-btn ltc-light-gray">QXC</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a href="logs.php" class="nav-link a-btn ltc-light-gray">Logs</a>
                                                        </li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>
</div>
<!-- bloc-0 END -->

<!-- bloc-1 -->

<?php
$akey = shell_exec('cat akey');
      if($akey == 0){
              echo '
<!-- bloc-1 -->
<div class="bloc l-bloc tc-light-gray" id="bloc-1">
        <div class="container bloc-lg">
                <div class="row">
                        <div class="col text-md-left text-center text-lg-center">
                                <div class="form-group" action="getkey.php" method="get">
                                          <label class="text-lg-center label-style">broadpeak.io API Key</label>
                                          <form name="form" action="getkey.php" method="get">
						<input class="form-control" required name="key" id="key" />
						<br>
	                                        <div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-md-left text-center">
        	                                	<button class="bloc-button btn btn-lg btn-block btn-blue-ryb btn-rd" type="submit">
                	                                	Next
                        	                	</button>
						</div>
					  </form>
                                </div>
                        </div>
                </div>
        </div>
</div>

<!-- bloc-1 END -->


<!-- bloc-2 END -->';
	}
?>
<!-- bloc-3 -->
<div class="bloc l-bloc" id="bloc-3">
	<div class="container bloc-lg bloc-lg-lg">
		<div class="row">
			<div class="col text-md-left text-center">
				<div class="divider-h">
				</div>
			</div>
		</div>
	</div>
</div>

<!-- bloc-3 END -->

<!-- bloc-5 END -->

<!-- bloc-5 -->

<!-- bloc-5 END -->

<!-- bloc-7 -->
<!--
<div class="bloc l-bloc" id="bloc-7">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col text-md-left text-center">
				<div class="divider-h">
				</div>
			</div>
		</div>
	</div>
</div>
-->
<!-- bloc-7 END -->
<!-- bloc-8 -->
<?php
$akey = shell_exec('cat akey');    
      if($akey == 1){
              echo '
<div class="bloc" id="bloc-8">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-md-left text-center">
				<h3 class="mg-md text-lg-left float-lg-none tc-islamic-green">
					<span class="fa fa-check-circle"></span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; broadpeak.io API Key
				</h3>
			</div>
		</div>
	</div>
</div>
<!-- bloc-8 END -->

<!-- bloc-9 -->
<div class="bloc l-bloc " id="bloc-9">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col text-md-left text-center">
				<div class="divider-h">
				</div>
			</div>
		</div>
	</div>
</div>';
      }
?>
<!-- bloc-9 END -->

<!-- ScrollToTop Button -->
<a class="bloc-button btn btn-d scrollToTop" onclick="scrollToTarget('1',this)"><svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 32 32"><path class="scroll-to-top-btn-icon" d="M30,22.656l-14-13-14,13"/></svg></a>
<!-- ScrollToTop Button END-->


</div>
<!-- Main container END -->
    


<!-- Additional JS -->
<script src="./js/jquery.min.js?6887"></script>
<script src="./js/bootstrap.bundle.min.js?2844"></script>
<script src="./js/blocs.min.js?4802"></script>
<script src="./js/lazysizes.min.js" defer></script><!-- Additional JS END -->


</body>
</html>
