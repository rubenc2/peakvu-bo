#!/bin/bash
while true
do
  api_key=`cat /var/www/html/keyfile | cut -d\' -f2`
  pxk=`cat /var/www/html/pxk | awk '{print $1 * 1}'`
  if ([[ ${api_key} != "" ]] || [[ ! -z ${api_key} ]]) && [[ ${pxk} -eq 0 ]]; then
  	echo "1" > /var/www/html/pxk
	echo "0" > /var/www/html/sdone
  	let s=0

	if [[ -f "/var/www/html/tmp/sources" ]]; then
	    rm /var/www/html/tmp/sources
	fi

	if [[ -f "/var/www/html/tmp/services" ]]; then
	    rm /var/www/html/tmp/service*
	fi

  	curl -s -X GET "https://api.broadpeak.io/v1/services" -H "accept: application/json" -H "Authorization: Bearer ${api_key}" > /var/www/html/response_services.json
  	NOS=`jq ".[].id" /var/www/html/response_services.json | wc -l | awk '{print $1-1}'`
  	for (( cs=0; cs<=${NOS}; cs++ )); do
		#echo $cs
  		type=`jq .[$cs].type /var/www/html/response_services.json | cut -d\" -f2`
		#echo ">${type}<"
  		if [[ ${type} == "content-replacement" ]]; then
  			let s=s+1
			#echo $s
  			sname=`jq ".[$cs].name" /var/www/html/response_services.json | cut -d\" -f2`
  			sid=`jq ".[$cs].id" /var/www/html/response_services.json`
  			echo "$s.- Service: ${sname} - ID: ${sid}" > /var/www/html/tmp/service${s}
                        echo "${sname},${sid}" >> /var/www/html/tmp/services
  		fi
		echo ${s} > /var/www/html/nos
  	done

        curl -s -X GET "https://api.broadpeak.io/v1/sources" -H "accept: application/json" -H "Authorization: Bearer ${api_key}" > /var/www/html/response_sources.json
        NOS=`jq ".[].id" /var/www/html/response_sources.json | wc -l | awk '{print $1-1}'`
        for (( cs=0; cs<=${NOS}; cs++ )); do
                
                type=`jq .[$cs].type /var/www/html/response_sources.json | cut -d\" -f2`
               
                if [[ ${type} == "live" ]]; then
                       
                        lsname=`jq ".[$cs].name" /var/www/html/response_sources.json | cut -d\" -f2`
                        lsid=`jq ".[$cs].id" /var/www/html/response_sources.json`
                        echo "${lsname},${lsid}" >> /var/www/html/tmp/sources
                fi
        done
	echo "1" > /var/www/html/sdone
	chown www-data:www-data /var/www/html/tmp/*
  fi
  sleep 5
done
