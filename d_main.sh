#!/bin/bash
while true
do
  file=`ls -la /var/www/html/tmp | grep ".csv" | awk '{print $9}'`
  px=`cat /var/www/html/px | awk '{print $1 * 1}'`
  sdone=`cat /var/www/html/sdone | awk '{print $1 * 1}'`
  if [[ ! -z ${file} ]] && [[ ${px} -eq 1 ]] && [[ $sdone -eq 1 ]]; then
      /var/www/html/conv_csv.sh /var/www/html/tmp/${file}
      let l=0
      echo "0" > /var/www/html/mdone
      api_key=`cat /var/www/html/keyfile | cut -d\' -f2`
      tdiff=`cat /var/www/html/tzd | awk '{print $1 * 1}'`
#      while read slot
      while IFS= read -r slot || [ -n "$slot" ]
      do
      	let l=l+1
      	if [[ ${l} -eq 1 ]]; then
      		continue
      	fi
      	
      	service=$(echo $slot | cut -d, -f14)
      	source=$(echo $slot | cut -d, -f12)
      	date=$(echo $slot | cut -d, -f2)
      	start=$(echo $slot | cut -d, -f4)
      	end=$(echo $slot | cut -d, -f5)
      	description=$(echo $slot | cut -d, -f6)
        
	#echo $date
      	#echo $start
      	#echo $end

      	ioservice=`cat /var/www/html/tmp/services | grep "${service}"`
        if [[ -z ${ioservice} ]]; then
        	ldate=`date`
        	echo "<span style=\"color: red\">$ldate - ERROR: Service Name [ ${service} ] does not match any Service in broadpeak.io</span>" >> /var/www/html/main.log
        	continue
        fi
        iosource=`cat /var/www/html/tmp/sources | grep "${source}"`
        if [[ -z ${iosource} ]]; then
        	ldate=`date`
        	echo "<span style=\"color: red\">$ldate - ERROR: Source Name [ ${source} ] does not match any Live Source in broadpeak.io</span>" >> /var/www/html/main.log
        	continue
        fi

        sourceid=`cat /var/www/html/tmp/sources | grep "${source}" | cut -d, -f2`
        serviceid=`cat /var/www/html/tmp/services | grep "${service}" | cut -d, -f2`

        tz=`cat /var/www/html/tzd | cut -d\' -f2 | awk '{print $1 * 1}'`
#Start Time
        shour=$(echo $start | cut -d: -f1 | awk '{print $1 * 1}')
	ehour=$(echo $end | cut -d: -f1 | awk '{print $1 * 1}')
	if [[ ${shour} -gt ${ehour} ]]; then
		let dd=1
	else
		let dd=0
	fi
        shour=$((shour+tz))
        if [[ ${shour} -ge 24 ]]; then
        	let shour=shour-24
        	let ds=1
        else
        	let ds=0
        fi
        if [[ ${shour} -lt 10 ]]; then
        	shour="0${shour}"
        fi
	if [[ ${shour} -eq 24 ]]; then
		shour="00"
	fi
        smin=$(echo $start | cut -d: -f2 | awk '{print $1 * 1}')
        if [[ ${smin} -lt 10 ]]; then
        	smin="0${smin}"
        fi
#End Time
        #ehour=$(echo $end | cut -d: -f1 | awk '{print $1 * 1}')
        ehour=$((ehour+tz))
        if [[ ${ehour} -ge 24 ]]; then
        	let ehour=ehour-24
        	let de=1
        else
        	let de=0
        fi
        if [[ ${ehour} -lt 10 ]]; then
        	ehour="0${ehour}"
        fi
	if [[ ${ehour} -eq 24 ]]; then
		ehour="00"
	fi
        emin=$(echo $end | cut -d: -f2 | awk '{print $1 * 1}')
        if [[ ${emin} -lt 10 ]]; then
        	emin="0${emin}"
        fi
#Start date
        sdated=$(echo $date | cut -d\/ -f2 | awk '{print $1 * 1}')
        sdated=$((sdated+ds))
        if [[ ${sdated} -lt 10 ]]; then
        	sdated="0${sdated}"
        fi
        sdatem=$(echo $date | cut -d\/ -f1 | awk '{print $1 * 1}')
        if [[ ${sdatem} -lt 10 ]]; then
        	sdatem="0${sdatem}"
        fi
        sdatey=$(echo $date | cut -d\/ -f3)
#End date
        edated=$(echo $date | cut -d\/ -f2 | awk '{print $1 * 1}')
        edated=$((edated+de+dd))
        if [[ ${edated} -lt 10 ]]; then
        	edated="0${edated}"
        fi
        edatem=$(echo $date | cut -d\/ -f1 | awk '{print $1 * 1}')
        if [[ ${edatem} -lt 10 ]]; then
        	edatem="0${edatem}"
        fi
        edatey=$(echo $date | cut -d\/ -f3)
        
        startT="${sdatey}-${sdatem}-${sdated}T${shour}:${smin}:00Z"
        endT="${edatey}-${edatem}-${edated}T${ehour}:${emin}:00Z"
#        echo $startT
#	 echo $endT
#Duration calculation
        sseconds=`date --date "${startT}" +%s`
        eseconds=`date --date "${endT}" +%s`
        duration=$((eseconds-sseconds))

#Slot request body        
        body="{
          \"name\": \"${service} - ${description}\",
          \"startTime\": \"${startT}\",
          \"duration\": ${duration},
          \"replacement\": {
          \"id\": ${sourceid}
            }
        }"

        #echo $body | jq
        #echo "------------------------------------- "
#cURL command to create a new slot
        curl -s --location --request POST "https://api.broadpeak.io/v1/services/content-replacement/${serviceid}/slots" -H "accept: application/json" -H "Authorization: Bearer ${api_key}" -H "Content-Type: application/json" -d "${body}" > /var/www/html/response_slot.json
        error=`cat /var/www/html/response_slot.json | jq -r .error`
        message=`cat /var/www/html/response_slot.json | jq -r .message`
	ldate=`date`
        if [[ ${error} == "null" ]]; then
           echo "${ldate} - INFO: The Slot Request for Service ${service}:${serviceid} with Replacement Source ${source} was Successful" >> /var/www/html/main.log
        else
           echo "<span style=\"color: red\">${ldate} - API ERROR: ${service}:${serviceid} | Replacement soure ${source}:${sourceid} | ${error} | ${message}</span>" >> /var/www/html/main.log
        fi
        sleep 1
      done < /var/www/html/tmp/${file}
      mv /var/www/html/tmp/${file} /var/www/html/tmp/$(echo $file | cut -d\. -f1).done
      echo "<span style=\"color: yellow\">++++++++++++++++++++++++ End Of Request ++++++++++++++++++++++++</span>" >> /var/www/html/main.log
      echo "1" > /var/www/html/mdone
  fi
  sleep 10
done

