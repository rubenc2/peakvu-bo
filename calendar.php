<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
	<link rel="canonical" href="simsub/calendar.php">
	<meta name="robots" content="index, follow">
    <link rel="shortcut icon" type="image/png" href="favicon.png">
    
	<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?9642">
	<link rel="stylesheet" type="text/css" href="style.css?8895">
	
    <title>PeakVu-BO</title>


    
<!-- Analytics -->
 
<!-- Analytics END -->
    
</head>
<body>

<!-- Preloader -->
<div id="page-loading-blocs-notifaction" class="page-preloader"></div>
<!-- Preloader END -->


<!-- Main container -->
<div class="page-container">
    
<!-- bloc-0 -->
<div class="bloc l-bloc" id="bloc-0">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col">
				<nav class="navbar navbar-light row navbar-expand-md flex-column" role="navigation">
					<a class="navbar-brand mx-auto" href="setup.php"><img src="img/Logotype%20white%20Broadpeak%20rgb.png" alt="logo" /></a>
					<button id="nav-toggle" type="button" class="ui-navbar-toggler navbar-toggler border-0 p-0" data-toggle="collapse" data-target=".navbar-34817" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse navbar-34817">
						<ul class="site-navigation nav navbar-nav mx-auto justify-content-center">
                                                        <li class="nav-item">
                                                                <a href="setup.php" class="nav-link a-btn ltc-light-gray">Setup</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a href="services.php" class="nav-link a-btn ltc-light-gray">Services</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a href="calendar.php" class="nav-link a-btn ltc-light-gray">Schedule</a>
							</li>
                                                        <li class="nav-item">
                                                                <a href="/bo/index.php" class="nav-link a-btn ltc-light-gray">QXC</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a href="logs.php" class="nav-link a-btn ltc-light-gray">Logs</a>
							</li>
                                                        <li class="nav-item">
                                                                <a href="/sim/index.php" class="nav-link a-btn ltc-light-gray">Simulator</a>
                                                        </li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>
</div>
<!-- bloc-0 END -->

<!-- bloc-11 -->
<div class="bloc" id="bloc-11">
	<div class="container bloc-lg bloc-sm-lg">
		<div class="row">
			<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-md-left text-center">
				<h2 class="mg-md text-lg-center tc-light-gray">
					Schedule
				</h2>
			</div>
		</div>
	</div>
</div>
<!-- bloc-11 END -->

<!-- bloc-12 -->
<div class="bloc l-bloc" id="bloc-12">
	<div class="container bloc-lg bloc-md-lg">
		<div class="row">
			<div class="col text-md-left text-center">
				<div >
<?php
$token=shell_exec('cat keyfile | sed s"/\'//"g');
echo "					<iframe src=\"schedule/events?hide=true&token={$token}\" width=\"100%\" height=\"1080\">
                                	</iframe>";
?>
					
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-12 END -->

<!-- bloc-13 -->
<!--
<div class="bloc l-bloc" id="bloc-13">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col text-md-left text-center">
				<div class="divider-h">
				</div>
			</div>
		</div>
	</div>
</div>
-->
<!-- bloc-13 END -->

<!-- bloc-14 -->

<div class="bloc l-bloc" id="bloc-14">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-md-left text-center">
<?php
$akey=shell_exec('cat akey');
if($akey == 0){
	exit ();
}else{
	echo '				<a href="start.php" class="btn btn-lg btn-block btn-rd btn-islamic-green" id="back"> S t a r t &nbsp &nbsp  O v e r </a>';
}
?>
			</div>
		</div>
	</div>
</div>

<!-- bloc-14 END -->

<!-- ScrollToTop Button -->
<a class="bloc-button btn btn-d scrollToTop" onclick="scrollToTarget('1',this)"><svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 32 32"><path class="scroll-to-top-btn-icon" d="M30,22.656l-14-13-14,13"/></svg></a>
<!-- ScrollToTop Button END-->


</div>
<!-- Main container END -->
    


<!-- Additional JS -->
<script src="./js/jquery.min.js?5757"></script>
<script src="./js/bootstrap.bundle.min.js?4670"></script>
<script src="./js/blocs.min.js?4044"></script>
<script src="./js/lazysizes.min.js" defer></script><!-- Additional JS END -->


</body>
</html>
